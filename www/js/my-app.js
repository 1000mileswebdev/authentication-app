// Initialize app
var myApp = new Framework7({
    swipeBackPage: false
});


// If we need to use custom DOM library, let's save it to $$ variable:
var $$ = Dom7;

// Add view
var mainView = myApp.addView('.view-main', {
    // Because we want to use dynamic navbar, we need to enable it for this view:
    dynamicNavbar: true
});

// Handle Cordova Device Ready Event
$$(document).on('deviceready', function() {
    console.log("Device is ready!");
    localStorage.setItem("deviceID", device.uuid);
});


// Now we need to run the code that will be executed only for About page.

// Option 1. Using page callback for page (for "about" page in this case) (recommended way):
myApp.onPageInit('about', function (page) {
    // Do something here for "about" page

})

// Option 2. Using one 'pageInit' event handler for all pages:
$$(document).on('pageInit', function (e) {
    // Get page data from event data
    var page = e.detail.page;
})

// Option 2. Using live 'pageInit' event handlers for each page
$$(document).on('pageInit', '.page[data-page="about"]', function (e) {
    // Following code will be executed for page with data-page attribute equal to "about"
    myApp.alert('Here comes About page');
})

function barcodeScanner(mode){

    var online = window.navigator.onLine;

    if(online !== true){
        myApp.alert("You are offline. Please make sure there is an internet connection available", 'Connection Error', function () {
            return false;
        });
    }else{

        if(mode == 1){

            var buttons = [{
                text: 'Select Database',
                label: true
            },
            {
                text: '1000 Miles Live',
                bold: false,
                onClick: function(){
                    var dbase = 'live';
                    scan(mode, dbase);
                }
            },
            {
                text: '1000 Miles Development',
                bold: false,
                onClick: function(){
                    var dbase = 'dev';
                    scan(mode, dbase);
                }
            }];

            myApp.actions(buttons);

        }else{
            scan(mode, false);
        }
    }

}

function scan(mode, db){
    var checkUser = localStorage.getItem("username");
    cordova.plugins.barcodeScanner.scan(
        function (result) {
            localStorage.setItem("qrCode", result.text);
            var qrResult = localStorage.getItem("qrCode");
            var devID = localStorage.getItem("deviceID");
            var userEmail = localStorage.getItem("email");
            if(result.cancelled == true){
            } else if(mode==1) { //FOR ZULU

                var zuluURL = "";

                switch(db){
                    case 'live':
                        zuluURL = 'http://zulu.1000miles.com.cn/verify/mobile_auth';
                        break;
                    case 'dev':
                        zuluURL = 'http://54.223.107.3:8069/verify/mobile_auth';
                        break;
                }

                $.ajax({
                    //url:'http://54.223.107.3/dev/info/auth/receive_auth',
                    url: zuluURL,
                    type:'GET',
                    data:{
                        email:userEmail,
                        qr_code:qrResult,
                        device_id:devID
                    },
                    success:function(data){
                        var result = JSON.parse(data);

                         if(result && result != null){
                            myApp.alert(result[0].message, result[0].title);
                         }
                    },
                    error:function(w, t, f){
                        myApp.alert("POSTING FAILED:\n" + w + ":\n" + t + ": " + f, 'Error');
                    }
                });

            } else { //FOR INFO
                $.ajax({
                    url:'http://1000miles.com.cn/dev/info/auth/receive_auth',
                    type:'POST',
                    data:{
                        user_name:checkUser,
                        qr_code:qrResult,
                        device_id:devID
                    },
                    success:function(data){
                        var result = JSON.parse(data);
                        if(result && result != null){
                            myApp.alert(result.message, result.title);
                        }
                    },
                    error:function(w, t, f){
                        myApp.alert("POSTING FAILED:\n" + w + ":\n" + t + ": " + f, 'Error');
                    }
                });
            }
        },
        function (error) {
            myApp.alert("Scanning failed: " + error, "Error Scanning");
        },
        {
            preferFrontCamera : false,
            showFlipCameraButton : true,
            showTorchButton : true,
            torchOn: true,
            prompt : "Place a barcode inside the scan area",
            resultDisplayDuration: 500,
            formats : "QR_CODE,PDF_417",
            orientation : "portrait",
            disableAnimations : false,
            disableSuccessBeep: false
        }
    );
}

function checkLogin(){

    event.preventDefault();

    var online = window.navigator.onLine;

    if(online !== true){
        myApp.alert("You are offline. Please make sure there is an internet connection available", 'Connection Error', function () {
            return false;
        });
    }else{
        $('.loader').show();

        var user = $("[name='username']").val();
        var pass = $("[name='password']").val();

        $.ajax({
            url: 'http://1000miles.com.cn/dev/info/auth/mobile_login_check',
            type: 'POST',
            data: {
                username:user,
                password:pass
            },
            success:function(data){
                if(data == 1){
                    myApp.alert("You have entered the wrong username or password", "Login Failed", function () {
                        window.location = "index.html";
                        return false;
                    });
                } else{
                    localStorage.setItem("email", data);
                    localStorage.setItem("approval", "logged-in");
                    localStorage.setItem("username", user);
                    window.location.href = "authenticate.html";
                }
            },
            error:function(w, t, f){
                myApp.alert("POSTING FAILEDS:\n" + JSON.stringify(w) + ":\n" + JSON.stringify(t) + ": " + f, "Error");
            }
        });

        return false;
    }
}

function logout(){
    localStorage.clear();
    localStorage.setItem("approval", null);
    window.location.href = "index.html";
}

function checkLoginStatus(){
    var loginStatus = localStorage.getItem("approval");
    if(loginStatus == "null"){
        window.location.href = "index.html";
    }
}

function copyDeviceID(){
    myApp.alert(localStorage.getItem("deviceID"));
}

function loginMobileApp(mode){
    var online = window.navigator.onLine;

    if(online !== true){
        myApp.alert("You are offline. Please make sure there is an internet connection available", 'Connection Error', function () {
            return false;
        });
    }else{
        if(mode == 1){

            var buttons = [{
                text: 'Select Database',
                label: true
            },
                {
                    text: '1000 Miles Live',
                    bold: false,
                    onClick: function(){
                        var dbase = 'live';
                        mobileLogin(mode, dbase);
                    }
                },
                {
                    text: '1000 Miles Development',
                    bold: false,
                    onClick: function(){
                        var dbase = 'dev';
                        mobileLogin(mode, dbase);
                    }
                }];

            myApp.actions(buttons);

        }else{
            mobileLogin(mode, false);
        }
    }
}

function mobileLogin(mode, dbase){

    var devID = localStorage.getItem("deviceID");

    if(mode == 1){
        var userEmail = localStorage.getItem("email");

    }else{
        var checkUser = localStorage.getItem("username");
        $.ajax({
            url:'http://1000miles.com.cn/dev/info/auth/receive_auth_mobile',
            type:'POST',
            data:{
                user_name: checkUser,
                device_id: devID
            },
            success:function(data){
                var result = JSON.parse(data);
                if(result && result != null){
                    myApp.alert(result.message, result.title);
                }
            },
            error:function(w, t, f){
                myApp.alert("POSTING FAILED:\n" + w + ":\n" + t + ": " + f, 'Error');
            }
        });
    }

}